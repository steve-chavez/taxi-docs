
# Requerimientos

*conductor   **conductor-cliente

## Cliente

-Editar direccion                             2 semanas
-Pedir taxi                                   1 semana
-Buscar taxi(coordenadas)                     1 semana 
-Generar alarma *                             1 semana                              
-Confirmar carrera *                          1 semana
-Notificar confirmacion de carrera            1 semana
-Ver ruta *                                   2 semanas
-Ver progreso de llegada                      2 semanas
-Alertar llegada *                            1 semana

## Conductor

-Confirmar pasajero a bordo **                2 semanas
-Finalizar viaje *                            1 semana
-Activar Disponibilidad *                     1 semana
-Perfil*(notificar cambio de telefono) *      2 semanas
-Historial de carreras *                      2 semanas
-Buscar carrera por fecha *                   1 semana
-Detalle de servicio *                        1 semana    
